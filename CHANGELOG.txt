==== Paypal Donate Block changelog.txt ====

Version 1.0 (01/10/2013)
-----------
  * Added README.txt
  * Added CHANGELOG.txt
  * Added email and email validation
  * Added donation purpose
  * Added block body to hold other content
  * Added uninstaller
