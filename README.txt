Welcome to Paypal Donate Block module.

I wrote this very simple module for one of my projects since I did not 
find the PayPal donate module I specifically need (I just needed a 
block that I can move around where I needed it.)

Once you have downloaded the module and saved in your modules directory, 
simply enable the module and a link to Configure the block will be provided. 
You can also configure it through the Blocks page.

The following information is available:

* Donation email - required
* Donation purpose - required (this will appear in the PayPal donation page)
* Currency - required
* Block body - optional

The PayPal Donation block body and button are wrapped in div class so you can 
customize them as you wish:

<div class="paypal-donate-body">{paypal donate block body}</div>
<div class="paypal-donate-form">{paypal donate button}</div>

To remove the module, just disable it and then uninstall.

Much of the inspiration came from the PayPal Donate module 
(http://drupal.org/project/paypal_donate) so here's to them!

Cheers!

Elinore Tenorio
e: elinore.tenorio@gmail.com
w: http://obitwaryo.com
